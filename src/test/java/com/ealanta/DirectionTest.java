package com.ealanta;

import org.junit.Test;

import org.junit.*;
import com.ealanta.Direction;
public class DirectionTest {
	

	@Test
	public void testLeft() {
		Assert.assertEquals(Direction.WEST, Direction.NORTH.left());
		Assert.assertEquals(Direction.NORTH, Direction.EAST.left());
		Assert.assertEquals(Direction.EAST, Direction.SOUTH.left());
		Assert.assertEquals(Direction.SOUTH, Direction.WEST.left());
	}
	
	@Test
	public void testRight() {
		Assert.assertEquals(Direction.EAST, Direction.NORTH.right());
		Assert.assertEquals(Direction.SOUTH, Direction.EAST.right());
		Assert.assertEquals(Direction.WEST, Direction.SOUTH.right());
		Assert.assertEquals(Direction.NORTH, Direction.WEST.right());
	}
}
