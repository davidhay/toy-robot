package com.ealanta;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CommandStreamSourceTest {
	
	private CommandStreamSource source;

	@Before
	public void setup() {
		source = new CommandStreamSource("commands.txt");
	}
	
	@Test
	public void testCommandStreamCount() {
		Assert.assertEquals(12, source.getCommandStream().count());
	}

	@Test
	public void testCommandStreamValues() {
		List<String> expected = Arrays.asList(
				"place 0,0,NORTH",//1
				"move",//2
				"report",//3
				"place 0,0,NORTH",//4
				"left",//5
				"report",//6
				"place 1,2,EAST",//7
				"move",//8
				"move",//9
				"left",//10
				"move",//11
				"report"//12
				);
				
		List<String> actual =  source.getCommandStream().map(Object::toString).collect(Collectors.toList());		
		Assert.assertEquals(expected, actual);
	}
}
