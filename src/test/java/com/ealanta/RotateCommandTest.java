package com.ealanta;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class RotateCommandTest {
	
	private BoardState initialState;
	private List<String> messages;
	private RotateCommand LEFT = new RotateLeftCommand();
	private RotateCommand RIGHT = new RotateRightCommand();
	
	@Before
	public void setup() {
		this.messages = new ArrayList<String>();
		Board board = new Board(1,1,(msg) -> messages.add(msg));
		this.initialState = new BoardState(board, new BoardPosition(0, 0, Direction.NORTH));
	}
	
	private BoardState checkRotation(BoardState currentState, 
			RotateCommand rotateCommand, 
			Direction expectedDirection) {
		CommandResult result = currentState.process(rotateCommand);
		Assert.assertTrue(result.isSuccess());
		Assert.assertNull(result.getErrorMessage());
		Assert.assertEquals(rotateCommand, result.getCommand());
		Assert.assertEquals(currentState, result.getPrevious());
	
		BoardState nextState = result.getCurrent();
		
		Assert.assertEquals(currentState.getBoard(), nextState.getBoard());
		Assert.assertEquals(currentState.getPosition().getX(), nextState.getPosition().getX());
		Assert.assertEquals(currentState.getPosition().getY(), nextState.getPosition().getY());
		
		Assert.assertEquals(expectedDirection, nextState.getPosition().getFacing());
		
		return nextState;
	}

	@Test
	public void testRotateLeft() {
		BoardState state1 = checkRotation(initialState, LEFT, Direction.WEST);
		BoardState state2 = checkRotation(state1, LEFT, Direction.SOUTH);
		BoardState state3 = checkRotation(state2, LEFT, Direction.EAST);
		checkRotation(state3, LEFT, Direction.NORTH);
	
	}
	
	@Test
	public void testRotateRight() {
		BoardState state1 = checkRotation(initialState, RIGHT, Direction.EAST);
		BoardState state2 = checkRotation(state1, RIGHT, Direction.SOUTH);
		BoardState state3 = checkRotation(state2, RIGHT, Direction.WEST);
		checkRotation(state3, RIGHT, Direction.NORTH);	
	}
	
	@Test
	public void testToString() {
		Assert.assertEquals("left", LEFT.toString());
		Assert.assertEquals("right", RIGHT.toString());
	}

}
