package com.ealanta;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

public class MoveCommandTest {

	private List<String> messages;
	private Board board;
	private static final MoveCommand MOVE_COMMAND = new MoveCommand();
	
	@Before
	public void setup() {
		this.messages = new ArrayList<String>();
		this.board = new Board(3,3,(msg) -> messages.add(msg));
	}
	
	private BoardState checkOkayMove(BoardState state, int expectedX, int expectedY) {
		CommandResult result = state.process(MOVE_COMMAND);
		Assert.assertEquals(MOVE_COMMAND, result.getCommand());
		Assert.assertEquals(state, result.getPrevious());
		Assert.assertTrue(result.isSuccess());
		Assert.assertNull(result.getErrorMessage());
		
		BoardState nextState = result.getCurrent();
		Assert.assertEquals(state.getPosition().getFacing(), nextState.getPosition().getFacing());
		Assert.assertEquals(expectedX, nextState.getPosition().getX());
		Assert.assertEquals(expectedY, nextState.getPosition().getY());
		return result.getCurrent();
	}
	
	private void checkInvalidMove(BoardState state) {
		CommandResult result = state.process(MOVE_COMMAND);
		Assert.assertEquals(MOVE_COMMAND, result.getCommand());
		Assert.assertEquals(state, result.getPrevious());
		Assert.assertEquals(state, result.getCurrent());
		Assert.assertFalse(result.isSuccess());
		Assert.assertEquals("That move would take you off the board.", result.getErrorMessage());
	}
	
	public BoardState getInitialState(Direction direction) {
		BoardState initialState = new BoardState(this.board,new BoardPosition(1, 1, direction));
		return initialState;
	}
	
	@Test
	public void testMovingUp() {
		BoardState state1 = checkOkayMove(getInitialState(Direction.NORTH), 1, 2);
		checkInvalidMove(state1);
	}
	
	@Test
	public void testMovingDown() {
		BoardState state1 = checkOkayMove(getInitialState(Direction.SOUTH), 1, 0);
		checkInvalidMove(state1);	
	}
	
	@Test
	public void testMovingRight() {
		BoardState state1 = checkOkayMove(getInitialState(Direction.EAST), 2, 1);
		checkInvalidMove(state1);	
	}
	
	@Test
	public void testMovingLeft() {
		BoardState state1 = checkOkayMove(getInitialState(Direction.WEST), 0, 1);
		checkInvalidMove(state1);	
	}

}
