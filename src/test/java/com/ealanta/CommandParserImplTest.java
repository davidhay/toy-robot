package com.ealanta;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CommandParserImplTest {

	private CommandParserImpl parser;

	@Before	
	public void setup() {
		this.parser = new CommandParserImpl();
	}
	
	@Test
	public void testMove() {
		Optional<Command> res = parser.parse(" MovE");
		Assert.assertTrue(res.isPresent());
		Command com = res.get();
		Assert.assertTrue(com instanceof MoveCommand);
	}
	
	@Test
	public void testReport() {
		Optional<Command> res = parser.parse(" RePoRt ");
		Assert.assertTrue(res.isPresent());
		Command com = res.get();
		Assert.assertTrue(com instanceof ReportCommand);
	}
	
	@Test
	public void testLeft() {
		Optional<Command> res = parser.parse(" Left ");
		Assert.assertTrue(res.isPresent());
		Command com = res.get();
		Assert.assertTrue(com instanceof RotateLeftCommand);
	}
	
	@Test
	public void testRight() {
		Optional<Command> res = parser.parse(" RighT ");
		Assert.assertTrue(res.isPresent());
		Command com = res.get();
		Assert.assertTrue(com instanceof RotateRightCommand);
	}
	
	
	@Test
	public void testPlace() {
		Optional<Command> res = parser.parse(" place 12 , 34, NorTH ");
		Assert.assertTrue(res.isPresent());
		Command com = res.get();
		Assert.assertTrue(com instanceof PlaceCommand);
		PlaceCommand pc = (PlaceCommand)com;
		Assert.assertEquals(12, pc.getX());
		Assert.assertEquals(34, pc.getY());
		Assert.assertEquals(Direction.NORTH, pc.getFacing());
	}

}
