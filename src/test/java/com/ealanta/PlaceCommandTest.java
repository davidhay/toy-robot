package com.ealanta;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PlaceCommandTest {

	private Board board = new Board(4,2);
	private BoardState initialState;
	private ArrayList<String> messages;
	
	@Before
	public void setup() {
		messages = new ArrayList<String>();
		board = new Board(4,2, msg -> messages.add(msg));
		initialState = new BoardState(board);
	}
	
	private BoardState checkPlaceGood(BoardState currentState, int x, int y, Direction direction) {
		PlaceCommand command = new PlaceCommand(x, y, direction);
		
		CommandResult result = currentState.process(command);
		Assert.assertTrue(result.isSuccess());
		Assert.assertNull(result.getErrorMessage());
		Assert.assertEquals(command, result.getCommand());
		Assert.assertEquals(currentState, result.getPrevious());
		
		Assert.assertEquals(board, result.getCurrent().getBoard());
		Assert.assertEquals(x, result.getCurrent().getPosition().getX());
		Assert.assertEquals(y, result.getCurrent().getPosition().getY());
		Assert.assertEquals(direction, result.getCurrent().getPosition().getFacing());
		return result.getCurrent();
	}
	
	private BoardState checkPlaceBad(BoardState currentState, int x, int y, Direction direction, String expectedMessage) {
		PlaceCommand command = new PlaceCommand(x, y, direction);
		
		CommandResult result = currentState.process(command);
		Assert.assertFalse(result.isSuccess());
		Assert.assertEquals(expectedMessage, result.getErrorMessage());
		Assert.assertEquals(command, result.getCommand());
		Assert.assertEquals(currentState, result.getPrevious());
		Assert.assertEquals(currentState, result.getCurrent());
		
		return result.getCurrent();
	}
	
	@Test
	public void testPlaceCommandGood() {
		
		BoardState state1 = checkPlaceGood(initialState, 0, 0, Direction.NORTH);
		BoardState state2 = checkPlaceGood(state1, 0, 1, Direction.EAST);
		BoardState state3 = checkPlaceGood(state2, 3, 1, Direction.WEST);
		BoardState state4 = checkPlaceGood(state3, 3, 0, Direction.SOUTH);
		checkPlaceGood(state4, 2, 0, Direction.WEST);
		
	}
	
	@Test
	public void testPlaceCommandBad() {		
		BoardState state1 = checkPlaceBad(initialState, -1, 0, Direction.NORTH, "The requested place(-1,0) is off the board sized -  w(4) x h(2)");
		BoardState state2 = checkPlaceBad(state1, 4, 1, Direction.EAST, "The requested place(4,1) is off the board sized -  w(4) x h(2)");
		BoardState state3 = checkPlaceBad(state2, 0, -1, Direction.WEST, "The requested place(0,-1) is off the board sized -  w(4) x h(2)");
		BoardState state4 = checkPlaceBad(state3, 0, 2, Direction.SOUTH, "The requested place(0,2) is off the board sized -  w(4) x h(2)");
		checkPlaceBad(state4, -1, -1, Direction.WEST, "The requested place(-1,-1) is off the board sized -  w(4) x h(2)");
		
	}

}
