package com.ealanta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class BoardTest {
		
	@Test
	public void testBadBoardSize() {
		checkBadBoardSize(0, 3);
		checkBadBoardSize(3, 0);
		checkBadBoardSize(0,  0);
		checkBadBoardSize(-1, 3);
		checkBadBoardSize(3, -1);
		checkBadBoardSize(-1,  -1);		
	}
	
	@Test
	public void testGoodBoardSize() {
		new Board(1,1);
		new Board(4,1);
		new Board(1,4);		
	}
	
	@Test
	public void testWidthAndHeight() {
		Board board = new Board(2, 3);
		Assert.assertEquals(2, board.getWidth());
		Assert.assertEquals(3, board.getHeight());
	}
	
	@Test
	public void testMessageReceiver() {
		List<String> items = new ArrayList<>();
		Board board = new Board(2, 3, msg -> items.add(msg));
		board.sendBoardMessage("msg1");
		board.sendBoardMessage("msg2");
		Assert.assertEquals(items, Arrays.asList("msg1","msg2"));
	}
	
	private void checkBadBoardSize(int width, int height) {
		try {
			new Board(width,height);
			Assert.fail("exception was expected");
		} catch(IllegalArgumentException ex) {
			Assert.assertTrue(ex.getMessage().endsWith("has to be at least 1"));
		}
	}
	
	@Test
	public void testIsValid() {
		
		//TEST WITHIN THE BOARD
		Board board = new Board(4,2);
		for(int w = 0 ; w < board.getWidth();w++) {
			for(int h=0; h < board.getHeight();h++) {
				Assert.assertTrue(board.isValid(new BoardPosition(w, h, Direction.NORTH)));
			}
		}
		
		//TEST LEFT OF BOARD
		for(int h=-1; h < board.getHeight()+1;h++) {
			Assert.assertFalse(board.isValid(new BoardPosition(-1, h, Direction.EAST)));
		}
		
		//TEST RIGHT OF BOARD
		for(int h=-1; h < board.getHeight()+1;h++) {
			Assert.assertFalse(board.isValid(new BoardPosition(board.getWidth(), h, Direction.WEST)));
		}

		//TEST ONTOP OF BOARD
		for(int w=-1; w < board.getWidth()+1;w++) {
			Assert.assertFalse(board.isValid(new BoardPosition(w, board.getHeight(), Direction.SOUTH)));
		}
		
		//TEST BENEATH OF BOARD
		for(int w=-1; w < board.getWidth()+1;w++) {
			Assert.assertFalse(board.isValid(new BoardPosition(w, -1, Direction.NORTH)));
		}
	}
	
	@Test(expected=IllegalStateException.class)
	public void testCannotMoveBeforePlace() {
		Board board = new Board(3, 3);
		BoardState state = new BoardState(board);
		
		state.process(new MoveCommand());
	}
	
	
	@Test(expected=IllegalStateException.class)
	public void testCannotRotateLeftBeforePlace() {
		Board board = new Board(3, 3);
		BoardState state = new BoardState(board);
		
		state.process(new RotateLeftCommand());
	}
	
	@Test(expected=IllegalStateException.class)
	public void testCannotRotateRightBeforePlace() {
		Board board = new Board(3, 3);
		BoardState state = new BoardState(board);
		
		state.process(new RotateRightCommand());
	}
	
	@Test(expected=IllegalStateException.class)
	public void testCannotReportBeforePlace() {
		Board board = new Board(3, 3);
		BoardState state = new BoardState(board);
		
		state.process(new ReportCommand());
	}
}
