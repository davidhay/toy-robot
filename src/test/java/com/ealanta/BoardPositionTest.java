package com.ealanta;
import java.util.Random;

import org.junit.Test;
import org.junit.Assert;
import org.junit.Before;
public class BoardPositionTest {

	private int x;
	private int y;

	@Before
	public void setup() {
		Random r = new Random(System.currentTimeMillis());
		x = r.nextInt(100)+1;
		y = r.nextInt(100)+1;
	}	
	
	@Test
	public void testLeft() {
		check(Direction.WEST, getPosition(Direction.NORTH).left());
		check(Direction.NORTH, getPosition(Direction.EAST).left());
		check(Direction.EAST, getPosition(Direction.SOUTH).left());
		check(Direction.SOUTH, getPosition(Direction.WEST).left());
	}
	
	@Test
	public void testRight() {
		check(Direction.EAST, getPosition(Direction.NORTH).right());
		check(Direction.SOUTH, getPosition(Direction.EAST).right());
		check(Direction.WEST, getPosition(Direction.SOUTH).right());
		check(Direction.NORTH, getPosition(Direction.WEST).right());
	}

	private void check(Direction expectedDirection, BoardPosition newPosition) {
		Assert.assertEquals(x, newPosition.getX());
		Assert.assertEquals(y, newPosition.getY());
		Assert.assertEquals(expectedDirection, newPosition.getFacing());			
	}
	
	private BoardPosition getPosition(Direction facing) {
		return new BoardPosition(x, y, facing);
	}
	
	
	private void checkMove(Direction facing, int expectedX, int expectedY){
		BoardPosition position = new BoardPosition(1, 1, facing);
		BoardPosition result = position.move();
		
		Assert.assertEquals(expectedX, result.getX());
		Assert.assertEquals(expectedY, result.getY());
		Assert.assertEquals(facing, result.getFacing());

	}
	
	@Test
	public void testMove() {
		checkMove(Direction.NORTH, 1, 2);
		checkMove(Direction.SOUTH, 1, 0);
		checkMove(Direction.EAST, 2, 1);
		checkMove(Direction.WEST, 0, 1);
	}

}
