package com.ealanta;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import org.junit.Before;
import org.junit.Test;
import org.junit.*;

public class ToyBoardProcessorTest {

	private List<String> messages;
	private Board board;
	private ToyRobotProcessor processor;
	private Stream<Command> commands;

	@Before
	public void setup() {
		messages = new ArrayList<>();
		board = new Board(5, 5, msg -> messages.add(msg));
		processor = new ToyRobotProcessor(board);
		commands = new CommandStreamSource("commands.txt").getCommandStream();
	}
	
	@Test
	public void testProcessCommands() {
		
		BoardState finalState = processor.processCommands(this.commands);
		
		Assert.assertEquals(3, finalState.getPosition().getX());
		Assert.assertEquals(3, finalState.getPosition().getY());
		Assert.assertEquals(Direction.NORTH, finalState.getPosition().getFacing());
		
		Assert.assertEquals(3, messages.size());
		
		List<String> expectedMessages = Arrays.asList(
				"0,1, NORTH",
				"0,0, WEST",
				"3,3, NORTH");
		Assert.assertEquals(expectedMessages, messages);
	}
}
