package com.ealanta;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;

public class PlaceCommandParserTest {

	private PlaceCommandParser parser = new PlaceCommandParser();
	
	private void check(int x, int y, Direction direction, String input) {
		Optional<Command> result = parser.parse(input);
		Assert.assertTrue(result.isPresent());
		PlaceCommand command = (PlaceCommand)result.get();
		Assert.assertEquals(x, command.getX());
		Assert.assertEquals(y, command.getY());
		Assert.assertEquals(direction, command.getFacing());
	}

	@Test
	public void testParseCommand() {
		check(1, 2, Direction.NORTH,    "place 1,2,NORTH");
		check(1, 2, Direction.NORTH,    "place 1 , 2 , NORTH");
		check(1, 2, Direction.WEST, "place 1 , 2 , West");
		check(1, 2, Direction.EAST, "place 1 , 2 , easT");
		check(123, 456, Direction.SOUTH, "place 123 , 456 , South");
	}
}
