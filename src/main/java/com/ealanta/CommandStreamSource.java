package com.ealanta;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.stream.Stream;

public class CommandStreamSource {

	private final CommandParserImpl parser;
	private final String filename;

	public CommandStreamSource(String filename){
		this.parser = new CommandParserImpl();
		this.filename = filename;
	}
	
	public Stream<Command> getCommandStream(){
		InputStream is = this.getClass().getClassLoader().getResourceAsStream(this.filename);
		BufferedInputStream bis = new BufferedInputStream(is);
		InputStreamReader reader = new InputStreamReader(bis, StandardCharsets.UTF_8);
		LineNumberReader lnr = new LineNumberReader(reader);

		return lnr.lines().map(parser::parse)
				.filter(Optional::isPresent)
				  .map(Optional::get);
	}
}
