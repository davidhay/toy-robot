package com.ealanta;

public class RotateRightCommand extends RotateCommand {

	@Override
	public BoardPosition getBoardPosition(BoardState state) {
		return state.getPosition().right();
	}

	@Override
	public String toString() {
		return "right";
	}
}
