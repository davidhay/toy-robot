package com.ealanta;

import java.util.Arrays;
import java.util.List;
import java.util.function.IntFunction;

public enum Direction {

	NORTH, EAST, SOUTH, WEST;
	
	private static final List<Direction> DIRECTIONS = Arrays.asList(NORTH, EAST, SOUTH, WEST);

	private static final IntFunction<Integer> ROTATE_LEFT  = (a) -> (a) - 1;
	private static final IntFunction<Integer> ROTATE_RIGHT  = (a) -> (a) + 1;

	public Direction left() {
		return rotate(ROTATE_LEFT);
	}
	
	public Direction right() {
		return rotate(ROTATE_RIGHT);
	}
	
	private Direction rotate(IntFunction<Integer> rotationDirection) {
		int position = DIRECTIONS.indexOf(this);
		int newPosition = Math.floorMod(rotationDirection.apply(position), DIRECTIONS.size());
		return DIRECTIONS.get(newPosition);
	}	
	
}
