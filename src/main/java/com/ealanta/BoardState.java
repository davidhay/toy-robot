package com.ealanta;

public class BoardState {
	
	private final Board board;
	
	private final BoardPosition position;

	public Board getBoard() {
		return board;
	}

	public BoardPosition getPosition() {
		return position;
	}
	
	public CommandResult process(Command command) {
		return command.process(this);
	}
				
	public BoardState(Board board) {
		this.board = board;
		this.position = null;
	}	
	
	public BoardState(Board board, BoardPosition position) {
		this.board = board;
		if(!board.isValid(position)) {
			throw new IllegalArgumentException("The position is invalid");
		}
		this.position = position;
	}
	
	public BoardState setNewPosition(BoardPosition newPosition) {
		return new BoardState(this.board, newPosition);
	}
	
}
