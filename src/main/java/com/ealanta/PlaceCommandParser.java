package com.ealanta;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PlaceCommandParser {

	private String PATTERN = "PLACE\\s(\\d+)\\s*,\\s*(\\d+)\\s*,\\s*(NORTH|EAST|SOUTH|WEST)";
	private Pattern pattern = Pattern.compile(PATTERN);
	
	public Optional<Command> parse(String input){
		if(input == null) {
			return Optional.empty();
		}
		String trimmed = input.toUpperCase().trim();
		Matcher matcher = pattern.matcher(trimmed);
		if(matcher.matches()) {
			int x = Integer.parseInt(matcher.group(1));
			int y = Integer.parseInt(matcher.group(2));
			Direction direction = Direction.valueOf(matcher.group(3));
			return Optional.of(new PlaceCommand(x, y, direction));
		}else {
			return Optional.empty();
		}
	}
}
