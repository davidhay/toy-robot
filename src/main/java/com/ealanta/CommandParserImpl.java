package com.ealanta;

import java.util.Optional;

public class CommandParserImpl implements CommandParser {

	private static final String LEFT = "left";
	private static final String RIGHT = "right";
	private static final String MOVE = "move";
	private static final String REPORT = "report";
	private static final String PLACE = "place";
	
	private static final Command LEFT_CMD = new RotateLeftCommand();
	private static final Command RIGHT_CMD = new RotateRightCommand();
	private static final Command MOVE_CMD = new MoveCommand();
	private static final Command REPORT_CMD = new ReportCommand();
	
	private final PlaceCommandParser placeCommandParser;
	
	
	public CommandParserImpl(){
		placeCommandParser = new PlaceCommandParser();
	}

	@Override
	public Optional<Command> parse(String line) {	
		if(line == null) {
			return Optional.empty();
		}
		String trimmed = line.trim();
		
		if(LEFT.equalsIgnoreCase(trimmed)) {
			return Optional.of(LEFT_CMD);
			
		} else if(RIGHT.equalsIgnoreCase(trimmed)) {
			return Optional.of(RIGHT_CMD);
			
		} else if(MOVE.equalsIgnoreCase(trimmed)) {
			return Optional.of(MOVE_CMD);
			
		} else if(REPORT.equalsIgnoreCase(trimmed)) {
			return Optional.of(REPORT_CMD);
			
		} else if(trimmed.toLowerCase().startsWith(PLACE)) {
			return placeCommandParser.parse(trimmed);
		} else {
			return Optional.empty();
		}
	}

}
