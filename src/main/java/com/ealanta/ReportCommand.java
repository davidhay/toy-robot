package com.ealanta;

public class ReportCommand implements Command  {

	@Override
	public CommandResult process(BoardState state) {
		BoardPosition position = state.getPosition();
		if(position == null) {
			throw new IllegalStateException("You need to PLACE on board first");
		}
		String msg = String.format("%d,%d, %s", 
				position.getX(),
				position.getY(),
				position.getFacing() );
		
		state.getBoard().sendBoardMessage(msg);
		
		return CommandResult.getSuccess(this, state, state);
	}
	
	@Override
	public String toString() {
		return "report";
	}

}
