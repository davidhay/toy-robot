package com.ealanta;

import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class ToyRobotProcessor {

	private Board board;

	public ToyRobotProcessor(Board board) {
		this.board = board;
	}
	
	public BoardState processCommands(Stream<Command> commands) {
		Predicate<Command> predCommand = command -> command instanceof PlaceCommand == false;
		Stream<Command> placeOnwards = dropWhile(predCommand, commands);			
		
		AtomicReference<BoardState> stateRef = new AtomicReference<>(new BoardState(board));
		
		placeOnwards.forEach(command -> {
			stateRef.set(getBoardState(stateRef.get(), command));	
		});
		return stateRef.get();
	}
	
	public BoardState getBoardState(BoardState state, Command command) {
		CommandResult result = state.process(command);
		return result.getCurrent();
	}
	
	static <T> Stream<T> dropWhile(Predicate<T> p, Stream<T> s) {
	    class MutableBoolean { boolean b; }
	    MutableBoolean inTail = new MutableBoolean();
	    return s.filter(i -> inTail.b || !p.test(i) && (inTail.b = true));
	}
}
