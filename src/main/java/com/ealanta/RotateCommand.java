package com.ealanta;

public abstract class RotateCommand implements Command {
	
	@Override
	public CommandResult process(BoardState state) {
		if(state.getPosition() == null) {
			throw new IllegalStateException("You need to PLACE on board first");
		}
		BoardState newState = state.setNewPosition(getBoardPosition(state));		
		return CommandResult.getSuccess(this, state, newState);
	}

	public abstract BoardPosition getBoardPosition(BoardState state);
}
