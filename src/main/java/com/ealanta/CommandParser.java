package com.ealanta;

import java.util.Optional;

public interface CommandParser {

	Optional<Command> parse(String line);
	
}
