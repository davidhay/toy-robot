package com.ealanta;

public class BoardPosition {

	private final int x;
	private final int y;
	private final Direction facing;	

	public BoardPosition(int x, int y, Direction facing) {
		this.x=x;
		
		this.y=y;
		
		if(facing == null) {
			throw new IllegalArgumentException("The Direction cannot be null");
		}
		this.facing = facing;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public Direction getFacing() {
		return facing;
	}
	
	public BoardPosition left() {
		return new BoardPosition(x, y, facing.left());
	}
	
	public BoardPosition right() {
		return new BoardPosition(x, y, facing.right());
	}
	
	public BoardPosition move() {
		int newX = this.x;
		int newY = this.y;
		switch (facing){
		case NORTH:
			newY++; break;
		case SOUTH:
			newY--; break;
		case EAST:
			newX++; break;
		case WEST:
			newX--; break;
		default:
			throw new IllegalStateException();
		}
		return new BoardPosition(newX, newY, facing);
	}
	
}
