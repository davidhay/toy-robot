package com.ealanta;

public interface Command {
	
	public CommandResult process(BoardState state);

}
