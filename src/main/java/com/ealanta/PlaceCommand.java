package com.ealanta;

public class PlaceCommand extends BoardPosition implements Command {

	public PlaceCommand(int x, int y, Direction facing) {
		super(x,y,facing);
	}

	@Override
	public CommandResult process(BoardState state) {
		Board board = state.getBoard();
		int x = getX();
		int y = getY();
		if(x >= 0 && x < board.getWidth() && y >= 0 && y < board.getHeight()){
			BoardState previous = state;
			BoardState current = previous.setNewPosition(this);
			return CommandResult.getSuccess(this, previous, current);
		} else {
			String msg = String.format("The requested place(%d,%d) is off the board sized -  w(%d) x h(%d)", getX(), getY(), board.getWidth(), board.getHeight());
			return CommandResult.getError(this, state, msg);
		}
	}

	@Override
	public String toString() {
		return String.format("place %d,%d,%s", getX(), getY(), getFacing());
	}
}
