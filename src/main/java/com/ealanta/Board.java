package com.ealanta;

import java.util.function.Consumer;

public class Board {

	private final int width;
	private final int height;
	private final Consumer<String> msgListener;
	
	public Board(int width, int height, Consumer<String> msgListener) {
		super();
		if(width < 1){
			throw new IllegalArgumentException("The width has to be at least 1");
		}
		if(height < 1){
			throw new IllegalArgumentException("The height has to be at least 1");
		}
		this.width = width;
		this.height = height;
		this.msgListener = msgListener;
	}
	
	public Board(int width, int height) {
		this(width, height, System.out::println);
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}	
	
	public void sendBoardMessage(String msg) {
		this.msgListener.accept(msg);
	}

	public boolean isValid(BoardPosition position) {
		int x = position.getX();
		int y = position.getY();
		return x >= 0 && x < this.width && 
				y >= 0 && y < this.height;
	}
}
