package com.ealanta;

public class CommandResult {
	private final BoardState previous;
	private final BoardState current;
	private final Command command;
	private final boolean success;
	private final String errorMessage;
	
	private CommandResult(Command command, BoardState previous, BoardState current,  boolean success, String error) {
		super();
		this.previous = previous;
		this.current = current;
		this.command = command;
		this.success = success;
		this.errorMessage = error;
	}
	
	public BoardState getPrevious() {
		return previous;
	}

	public BoardState getCurrent() {
		return current;
	}

	public Command getCommand() {
		return command;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public static CommandResult getSuccess(Command command, BoardState previous, BoardState current) {
		return new CommandResult(command, previous, current, true, null);
	}
	
	public static CommandResult getError(Command command, BoardState previous, String error) {
		return new CommandResult(command, previous, previous, false, error);
	}
	
}
