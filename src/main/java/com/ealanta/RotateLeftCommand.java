package com.ealanta;

public class RotateLeftCommand extends RotateCommand {

	@Override
	public BoardPosition getBoardPosition(BoardState state) {
		return state.getPosition().left();
	}
	
	@Override
	public String toString() {
		return "left";
	}

}
