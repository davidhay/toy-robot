package com.ealanta;

public class MoveCommand implements Command {

	@Override
	public CommandResult process(BoardState state) {
		if(state.getPosition() == null) {
			throw new IllegalStateException("You need to PLACE on board first");
		}
		BoardPosition newPosition = state.getPosition().move();
		final CommandResult result;
		if(state.getBoard().isValid(newPosition)) {
			BoardState newState = state.setNewPosition(newPosition);
			result = CommandResult.getSuccess(this, state, newState);
		} else {
			result = CommandResult.getError(this,state,"That move would take you off the board.");
		}
		return result;
	}

	@Override
	public String toString() {
		return "move";
	}
}
