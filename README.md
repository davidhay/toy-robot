Toy Robot
=========

Pre Interview Test
---------------------

This is a pre interview test.
The instuctions are located here https://zone.github.io/backend/toy-robot

### Design

I decided to use quite a lot of simple clases. The idea being that they can be easily tested.
I've also gone for immutable data structures - again these are easy to test and are thread safe.

### Tests

I have managed to achieve a high level of test coverage (>90%). Tested using the EclEmma plugin.

To run all the tests:
`mvn clean test`

### Running the Code

The test class which runs the complete code is located in 

`src/test/java/com/ealanta/ToyBoardProcessorTest.java`

it processes the list of commands from the 

`src/main/resources/commands.txt` 

file - checks the final board state and checks the the 'report' commands output expected messages.

### Future Improvements

- It would be better if I'd used Singletons for Commands that don't contain any data.
- I didn't test that commands until the PLACE command are ignored - although there is code for this.
- I managed to make the REPORT command somewhat flexible so that it can be customised - this code not be in the best place though.

